# Asset Identity Management #


An individual visits the Holm portal and completes an Identity Validation Process customized to the Holm Business Customer requirements. This process verifies Personally Identifiable Information (PII) to ensure ownership of the identity with enough data to establish the level of trust required by the Holm Business Customer.  In other words, more PII may be collected to establish a high level of trust, e.g. scanning of title, deeds, the bill of sale, affidavit of title and transfer tax declarations while only minimal PII, e.g. only email, mobile phone number, house title may be collected for new users when the Holm Business Customer only wants to verify the user is real and unique.

After validation, the user is now considered a Holm Member with authenticated identity data secured in the Holm Application on the user’s device, not stored by Holm. The Holm Member may share this previously authenticated identity data with Holm Business Customers, businesses that enter into a partnership with Holm. Holm Business Customers leverage our blockchain technology for real-time authentication of Holm Member identity data.


## Identity Validation & Monetization ##

### Holm Identity Validation ###

Once identity data is provided by a user, Holm uses multiple identity validation service providers to authenticate submitted data against phone, credit, social media, and other public records. By combining many sources with fraud detection algorithms, manual auditing, and our proprietary internal decision engine, Holm maintains a high pass rate for legitimate houses while mitigating the risks of fraudulent behavior.

### Third Party Identity Validation ###

A Holm Business Customer may decide to accept identity data previously validated by a Holm Identity Partner rather than Holm. For example, real estate provider may trust the identity validation process of a financial institution, therefore accepting this data for their identity requirements. Another example is a loan provider using the Holm platform to issue digital identity data directly to the Holm portal, such as the note, the mortgage, loan application, loan estimates and closing disclosures.

### Monetize Reuseable KYC ###

When house is validated as a Holm Member, their identity can be reused at other Holm Business Customers, replacing some or all legacy identity validation.  This can provide Holm Identity Partners opportunities to monetize verified identity data of their houses. If you issue authenticated identity data on the blockchain with the Holm architecture, the Holm Member can share that data with a Holm Business Customer for real-time authentication of identity. The Holm Business Customer pays a small fee that is shared with the the Holm Identity Partner to avoid paying a larger fee to do their own authentication.
